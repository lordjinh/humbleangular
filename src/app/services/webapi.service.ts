import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ApiurlService } from './apiurl.service';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { catchError, retry, map } from 'rxjs/operators';
import { User } from '../modal/user';
import { Restaurant } from '../modal/restaurant';


@Injectable({
  providedIn: 'root'
})
export class WebapiService {

  isLoggedIn = false;
  token: any;
  username: any;

  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<User>;
  public allrestaurant: Observable<Restaurant>;
  apierror: any;
  apimsg: any;

  constructor(
    private http: HttpClient,
    private router: Router,
    private env: ApiurlService,
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();

   }

   public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  register(firstName, lastName, email, password) {
    return this.http.post<any>(this.env.API_ENDPOINT_REGISTER,
      {
        first_name: firstName,
        last_name: lastName,
        email,
        password
      })
        .pipe(map(user => {
          console.log('webapi service login ==:', user.error);
          if (user.error) {
            this.apierror = user.error;
            this.apimsg = user.message;
            this.router.navigateByUrl('');
            this.isLoggedIn = false;
          } else {
            this.router.navigateByUrl('/dashboard');
            localStorage.setItem('token', user.token);
            localStorage.setItem('currentUser', JSON.stringify(user));

            this.currentUserSubject.next(user);
            this.token = user.token;
            this.isLoggedIn = true;
            return user;
          }
        }));
  }

  login(email, password) {
    return this.http.post<any>(this.env.API_ENDPOINT_LOGIN, { email, password })
        .pipe(map(user => {
          console.log('webapi service login ==:', user.error);
          if (user.error) {
            this.apierror = user.error;
            this.apimsg = user.message;
            this.router.navigateByUrl('');
            this.isLoggedIn = false;
          } else {
            this.router.navigateByUrl('/dashboard');
            localStorage.setItem('token', user.token);
            localStorage.setItem('currentUser', JSON.stringify(user));

            this.currentUserSubject.next(user);
            this.token = user.token;
            this.isLoggedIn = true;
            return user;
          }
        }));
  }

  getAll() {
    return this.http.get<User[]>(this.env.API_ENDPOINT_USER_DETAILS);
  }

  logout() {
  // remove user from local storage and set current user to null
  localStorage.removeItem('currentUser');
  this.currentUserSubject.next(null);
}



restaurant(restaurantname, restauranttype) {
  return this.http.post<any>(this.env.API_ENDPOINT_CREATE_RESTAURANT,
    {
      restaurantname,
      restauranttype
    })
      .pipe(map(res => {
        console.log('webapi service login ==:', res.error);
        if (res.error) {
          this.apierror = res.error;
          this.apimsg = res.message;
          // this.router.navigateByUrl('/restaurant');
        } else {
          this.apimsg = res.message;
          return res;
        }
      }));
}


getAllRestaurants() {
  return this.http.get<Restaurant[]>(this.env.API_ENDPOINT_GET_RESTAURANT);
}


}
