import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiurlService {
  API_URL = environment.DOMAINURl;
  API_ENDPOINT_REGISTER = `${this.API_URL}/api/user/`;
  API_ENDPOINT_USER_DETAILS = `${this.API_URL}/api/user/`;
  API_ENDPOINT_LOGIN = `${this.API_URL}/api/user/login/`;
  API_ENDPOINT_GET_RESTAURANT = `${this.API_URL}/api/restaurant/`;
  API_ENDPOINT_CREATE_RESTAURANT = `${this.API_URL}/api/createrestaurant/`;

  constructor() { }
}
