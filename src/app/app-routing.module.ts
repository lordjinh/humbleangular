import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginsignupComponent } from './pages/loginsignup/loginsignup.component';
import { AboutComponent } from './pages/about/about.component';
import { SettingComponent } from './pages/setting/setting.component';
import { AuthGuardService } from './guards/auth-guard.service';
import { RestaurantComponent } from './pages/restaurant/restaurant.component';

const routes: Routes = [
  {path: '', component: LoginsignupComponent},
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService]},
  {path: 'restaurant', component: RestaurantComponent, canActivate: [AuthGuardService]},
  {path: 'about', component: AboutComponent, canActivate: [AuthGuardService]},
  {path: 'setting', component: SettingComponent, canActivate: [AuthGuardService]},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
