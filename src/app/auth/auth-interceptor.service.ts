import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { WebapiService } from '../services/webapi.service';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor(
    private apiEnpoint: WebapiService,
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    const currentUser = this.apiEnpoint.currentUserValue;
    console.log(currentUser);
    if (currentUser && currentUser.token) {
        request = request.clone({
            setHeaders: {
                Authorization: `HUMBLEAPI ${currentUser.token}`
            }
        });
    }
    return next.handle(request);

}
}
