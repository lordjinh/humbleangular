import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { WebapiService } from '../services/webapi.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    private router: Router,
    private apiEnpoint: WebapiService,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.apiEnpoint.isLoggedIn;
    if (currentUser) {
        // logged in so return true
        return true;
    }
    this.router.navigate([''], { queryParams: { returnUrl: state.url } });
    return false;
}

}
