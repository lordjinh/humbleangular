import { Component, OnInit } from '@angular/core';
import { WebapiService } from 'src/app/services/webapi.service';
import { User } from 'src/app/modal/user';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  loading = false;
  users: User[];
  currentUser: User;
  constructor(
    private router: Router,
    private apiEnpoint: WebapiService,
  ) {
    this.apiEnpoint.currentUser.subscribe(x => this.currentUser = x);
   }

   logout() {
    this.apiEnpoint.logout();
    this.router.navigate(['']);
}


  ngOnInit() {
    this.loading = true;
    this.apiEnpoint.getAll().pipe(first()).subscribe(users => {
        this.loading = false;
        this.users = users;
        console.log('dashboard--> userdetails : ', this.users);
    });
  }

}
