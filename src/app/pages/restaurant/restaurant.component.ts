import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WebapiService } from 'src/app/services/webapi.service';
import { Restaurant } from 'src/app/modal/restaurant';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.scss']
})
export class RestaurantComponent implements OnInit {

  restaurantForm: FormGroup;
  submitted = false;
  returnUrl: string;
  error: string;
  loading = false;
  allrestaurants: any;
  delbutton = false;
  delname: string;
  createaction = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private apiEnpoint: WebapiService,
  ) { }

  restaurant() {
    const val = this.restaurantForm.value;
    this.submitted = true;
    // stop here if form is invalid
    if (this.restaurantForm.invalid) {
      return;
    }
    if (val.restaurantname && val.restauranttype) {
        this.apiEnpoint.restaurant(val.restaurantname, val.restauranttype)
            .subscribe(
                () => {
                  if (this.apiEnpoint.apierror) {
                    this.error = this.apiEnpoint.apimsg;
                    this.loading = false;
                  } else {
                    this.error = this.apiEnpoint.apimsg;
                    console.log('Restuarant Added');
                  }
                },
                error => {
                    // this.error = error.message;
                    this.loading = false;
                }
            );
    }
  }

  deleteaction(res: string) {
    this.delbutton = true;
    this.createaction = false;
    this.delname = res;
  }

  createres() {
    this.createaction = true;
    this.delbutton = false;
  }

  cancelres() {
    this.createaction = false;
  }

  delcancel() {
    this.delbutton = false;
  }

  ngOnInit() {

    this.restaurantForm = this.formBuilder.group({
      restaurantname: [''],
      restauranttype: [''],
    });

    this.apiEnpoint.getAllRestaurants().pipe().subscribe(res => {
      this.loading = false;
      this.allrestaurants = res['restaurant'];
      console.log('restaurant--> allrestaurant : ', this.allrestaurants);
  });
  }

}
