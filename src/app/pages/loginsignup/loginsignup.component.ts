import { Component, OnInit } from '@angular/core';
import { WebapiService } from 'src/app/services/webapi.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-loginsignup',
  templateUrl: './loginsignup.component.html',
  styleUrls: ['./loginsignup.component.scss']
})
export class LoginsignupComponent implements OnInit {

  loginsignupForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string;
  showbutton = false;
  showeye = false;

  constructor(
    private formBuilder: FormBuilder,
    private apiEnpoint: WebapiService,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  register() {
    const val = this.loginsignupForm.value;
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginsignupForm.invalid) {
      return;
    }
    if (val.email && val.password) {
        this.apiEnpoint.register(val.firstName, val.lastName, val.email, val.password)
            .subscribe(
                () => {
                  if (this.apiEnpoint.apierror) {
                    this.error = this.apiEnpoint.apimsg;
                    this.loading = false;
                  } else {
                  console.log('User is logged in');
                  this.router.navigateByUrl('/dashboard');
                  }
                },
                error => {
                    // this.error = error.message;
                    this.loading = false;
                }
            );
    }
  }

  login() {
    const val = this.loginsignupForm.value;
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginsignupForm.invalid) {
      return;
    }
    if (val.email && val.password) {
        this.apiEnpoint.login(val.email, val.password)
            .subscribe(
                () => {
                  if (this.apiEnpoint.apierror) {
                    this.error = this.apiEnpoint.apimsg;
                    this.loading = false;
                  } else {
                  console.log('User is logged in');
                  this.router.navigateByUrl('/dashboard');
                  }
                },
                error => {
                    // this.error = error.message;
                    this.loading = false;
                }
            );
    }
  }

  showPassword() {
    this.showbutton = !this.showbutton;
    this.showeye = !this.showeye;
  }

ngOnInit() {

  this.loginsignupForm = this.formBuilder.group({
    firstName: [''],
    lastName: [''],
    email: ['', Validators.required],
    password: ['', Validators.required]
  });

  const signUpButton = document.getElementById('signUp');
  const signInButton = document.getElementById('signIn');
  const container = document.getElementById('container');
  signUpButton.addEventListener('click', () => container.classList.add('right-panel-active')
  );
  signInButton.addEventListener('click', () => container.classList.remove('right-panel-active')
  );


  }

  // convenience getter for easy access to form fields
  get f() { return this.loginsignupForm.controls; }

}
