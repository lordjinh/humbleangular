import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  allhighcharts = Highcharts;

  allchartOptions = {
    exporting: {
      buttons: {
          contextButton: {
              menuItems: [{
                  textKey: 'downloadXLS',
                  onclick() {
                      this.downloadXLS();
                  }
              }, {
                  textKey: 'downloadCSV',
                  onclick() {
                      this.downloadCSV();
                  }
              }]
          }
      }
  },
     chart: {
        type: 'spline'
     },
     title: {
        text: 'All Resturants total average rating'
     },
     subtitle: {
        text: '(rating as per each day)'
     },
     xAxis: {
        categories: ['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat',
           'Sun']
     },
     yAxis: {
        title: {
           text: 'Rating'
        }
     },
     tooltip: {
        valueSuffix: '*'
     },
     series: [
        {
            name: 'Beach Cafe',
            data: [4.1, 3.0, 4.0, 5.0, 4.0, 4.0, 5.0]
        },
        {
            name: 'Indian Restaurant',
            data: [4.8, 4.0, 4.0, 4.3, 4.1, 5.0, 5.0]
        },
        {
            name: 'Tesla Pub',
            data: [3.8, 3.0, 3.0, 3.3, 2.0, 4.0, 4.0]
        },
        {
            name: 'Seaside Cafe',
            data: [4.1, 4.0, 5.0, 3.3, 4.3, 4.0, 5.0]
        }
     ]
  };

  chartOptions1 = {
    exporting: {
      buttons: {
          contextButton: {
              menuItems: [{
                  textKey: 'downloadXLS',
                  onclick() {
                      this.downloadXLS();
                  }
              }, {
                  textKey: 'downloadCSV',
                  onclick() {
                      this.downloadCSV();
                  }
              }]
          }
      }
  },
     chart: {
        type: 'spline'
     },
     title: {
        text: 'Beach Cafe average rating'
     },
     subtitle: {
        text: '(rating as per each day)'
     },
     xAxis: {
        categories: ['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat',
           'Sun']
     },
     yAxis: {
        title: {
           text: 'Rating'
        }
     },
     tooltip: {
        valueSuffix: '*'
     },
     series: [
        {
            name: 'Beach Cafe',
            data: [4.1, 3.0, 4.0, 5.0, 4.0, 4.0, 5.0]
        }
     ]
  };

  chartOptions2 = {
    exporting: {
      buttons: {
          contextButton: {
              menuItems: [{
                  textKey: 'downloadXLS',
                  onclick() {
                      this.downloadXLS();
                  }
              }, {
                  textKey: 'downloadCSV',
                  onclick() {
                      this.downloadCSV();
                  }
              }]
          }
      }
  },
     chart: {
        type: 'spline'
     },
     title: {
        text: 'Indian Restaurant average rating'
     },
     subtitle: {
        text: '(rating as per each day)'
     },
     xAxis: {
        categories: ['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat',
           'Sun']
     },
     yAxis: {
        title: {
           text: 'Rating'
        }
     },
     tooltip: {
        valueSuffix: '*'
     },
     series: [
        {
            name: 'Indian Restaurant',
            data: [4.1, 4.0, 5.0, 3.3, 4.3, 4.0, 5.0],
            color: 'black',
        }
     ]
  };

  chartOptions3 = {
    exporting: {
      buttons: {
          contextButton: {
              menuItems: [{
                  textKey: 'downloadXLS',
                  onclick() {
                      this.downloadXLS();
                  }
              }, {
                  textKey: 'downloadCSV',
                  onclick() {
                      this.downloadCSV();
                  }
              }]
          }
      }
  },
     chart: {
        type: 'spline'
     },
     title: {
        text: 'Tesla Pub average rating'
     },
     subtitle: {
        text: '(rating as per each day)'
     },
     xAxis: {
        categories: ['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat',
           'Sun']
     },
     yAxis: {
        title: {
           text: 'Rating'
        }
     },
     tooltip: {
        valueSuffix: '*'
     },
     series: [
        {
            name: 'Tesla Pub',
            data: [3.8, 3.0, 3.0, 3.3, 2.0, 4.0, 4.0],
            color: '#00FF00'
        }
     ]
  };

  chartOptions4 = {
    exporting: {
      buttons: {
          contextButton: {
              menuItems: [{
                  textKey: 'downloadXLS',
                  onclick() {
                      this.downloadXLS();
                  }
              }, {
                  textKey: 'downloadCSV',
                  onclick() {
                      this.downloadCSV();
                  }
              }]
          }
      }
  },
     chart: {
        type: 'spline'
     },
     title: {
        text: 'Seaside Cafe average rating'
     },
     subtitle: {
        text: '(rating as per each day)'
     },
     xAxis: {
        categories: ['Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat',
           'Sun']
     },
     yAxis: {
        title: {
           text: 'Rating'
        }
     },
     tooltip: {
        valueSuffix: '*'
     },
     series: [
        {
          name: 'Seaside Cafe',
            data: [4.1, 4.0, 5.0, 3.3, 4.3, 4.0, 5.0],
            color: 'orange'
        }
     ]
  };

ngOnInit() {

  }

}
